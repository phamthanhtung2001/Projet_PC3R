package filters;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

import java.io.IOException;

public class JWTFilter implements Filter {
    private static final String SECRET_KEY = "Test";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        String authorizationHeader = httpRequest.getHeader("Authorization");
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            System.out.println("Missing or invalid Authorization header");
            httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
            return;
        }
        //Test
        String token = authorizationHeader.substring(7); // Extract token
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer("auth0")
                    .build(); // Reusable verifier instance
            DecodedJWT jwt = verifier.verify(token);

            // Log the token validation
            System.out.println("Token validated successfully for user: " + jwt.getClaim("username").asString());

            // Optionally set user details in request attributes for further processing
            httpRequest.setAttribute("username", jwt.getClaim("username").asString());
            httpRequest.setAttribute("role", jwt.getClaim("role").asString());
            httpRequest.setAttribute("jwt", token);

            chain.doFilter(request, response); // Proceed with the request
        } catch (JWTVerificationException exception) {
            System.out.println("Token validation failed: " + exception.getMessage());
            httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
        }
    }

    @Override
    public void destroy() {
    }
}
