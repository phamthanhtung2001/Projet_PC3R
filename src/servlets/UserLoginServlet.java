package servlets;

import jakarta.annotation.Resource;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import constants.IOnlineBookStoreConstants;
import sql.IUserConstants;

import java.io.*;
import java.sql.*;
import javax.sql.DataSource;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = -7773629178766994810L;
	@Resource(name="jdbc/Bookworm") //load resource file- context.xml
	private DataSource datasource;  //Creating DataSource object
	private static final String SECRET_KEY = "Test";

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		PrintWriter pw = res.getWriter();
		res.setContentType(IOnlineBookStoreConstants.CONTENT_TYPE_TEXT_HTML);
		String uName = req.getParameter(IUserConstants.COLUMN_USERNAME);
		String pWord = req.getParameter(IUserConstants.COLUMN_PASSWORD);
		
		try {
			Connection con = datasource.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM " + IUserConstants.TABLE_USERS + " WHERE "
					+ IUserConstants.COLUMN_USERNAME + "=? AND " + IUserConstants.COLUMN_PASSWORD + "=? AND " + IUserConstants.COLUMN_USERTYPE + "=2");
			ps.setString(1, uName);
			ps.setString(2, pWord);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {
				Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY);
                String token = JWT.create()
                        .withIssuer("auth0")
                        .withClaim("username", uName)
                        .withClaim("role", "user")
                        .sign(algorithm);
                
                // Log the generated token
                System.out.println("Generated JWT: " + token);

                // Set the token in the response header
                res.setHeader("Authorization", "Bearer " + token);
                
                // Send the token and success message back to the client
                pw.println("<script>");
                pw.println("localStorage.setItem('jwt', '" + token + "');");
                pw.println("window.location.href = 'UserHome.html';");
                pw.println("</script>");
			} else {
				RequestDispatcher rd = req.getRequestDispatcher("UserLogin.html");
				rd.include(req, res);
				pw.println("<div class=\"tab\">Incorrect UserName or PassWord</div>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
