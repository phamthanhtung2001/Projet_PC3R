package servlets;
import java.io.*;
import java.sql.*;

import javax.sql.DataSource;

import jakarta.annotation.Resource;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;

import sql.IBookConstants;

public class ViewBookServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6565685437984898078L;
	@Resource(name="jdbc/Bookworm") //load resource file- context.xml
	private DataSource datasource;  //Creating DataSource object
	public void service(HttpServletRequest req,HttpServletResponse res) throws IOException,ServletException
	{
		PrintWriter pw = res.getWriter();
		res.setContentType("text/html");
		try {
			Connection con = datasource.getConnection();
			PreparedStatement ps = con.prepareStatement("Select * from " + IBookConstants.TABLE_BOOK);
			ResultSet rs = ps.executeQuery();
			RequestDispatcher rd = req.getRequestDispatcher("ViewBooks.html");
			rd.include(req, res);
			pw.println("<div class=\"tab\">Books Available In Our Store</div>");
			pw.println("<div class=\"tab\">\r\n" + 
					"		<table>\r\n" + 
					"			<tr>\r\n" + 
					"				\r\n" + 
					"				<th>Book Code</th>\r\n" + 
					"				<th>Book Name</th>\r\n" + 
					"				<th>Book Author</th>\r\n" + 
					"				<th>Book Price</th>\r\n" + 
					"				<th>Quantity</th><br/>\r\n" + 
					"			</tr>");
			while(rs.next())
			{
			    String bCode = rs.getString(1);
			    String bName = rs.getString(2);
			    String bAuthor = rs.getString(3);
			    int bPrice = rs.getInt(4);
			    int bQty = rs.getInt(5);
			    pw.println("<tr><td>"+bCode+"</td>");
			    pw.println("<td><a href=\"BookDetailServlet?title="+bName+"\">"+bName+"</a></td>");
			    pw.println("<td>"+bAuthor+"</td>");
			    pw.println("<td>"+bPrice+"</td>");
			    pw.println("<td>"+bQty+"</td></tr>");
			}
			pw.println("</table>\r\n" + 
					"	</div>");
			//pw.println("<div class=\"tab\"><a href=\"AddBook.html\">Add More Books</a></div>");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
