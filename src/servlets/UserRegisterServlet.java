package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.sql.DataSource;

import constants.IOnlineBookStoreConstants;
import jakarta.annotation.Resource;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import sql.IUserConstants;
 

public class UserRegisterServlet extends HttpServlet {
 
    /**
	 * 
	 */
	private static final long serialVersionUID = 7566400118928680314L;
	@Resource(name="jdbc/Bookworm") //load resource file- context.xml
	private DataSource datasource;  //Creating DataSource object
	protected void doPost(HttpServletRequest req,
            HttpServletResponse res) throws ServletException, IOException {
		PrintWriter pw = res.getWriter();
		res.setContentType(IOnlineBookStoreConstants.CONTENT_TYPE_TEXT_HTML);

		String uName = req.getParameter(IUserConstants.COLUMN_USERNAME);
		String pWord = req.getParameter(IUserConstants.COLUMN_PASSWORD);
		String fName = req.getParameter(IUserConstants.COLUMN_FIRSTNAME);
		String lName = req.getParameter(IUserConstants.COLUMN_LASTNAME);
		String addr = req.getParameter(IUserConstants.COLUMN_ADDRESS);
		String phNo = req.getParameter(IUserConstants.COLUMN_PHONE);
		String mailId = req.getParameter(IUserConstants.COLUMN_MAILID);
		try {
			Connection con = datasource.getConnection();
			PreparedStatement ps = con
					.prepareStatement("insert into " + IUserConstants.TABLE_USERS + "  values(?,?,?,?,?,?,?,?)");
			ps.setString(1, uName);
			ps.setString(2, pWord);
			ps.setString(3, fName);
			ps.setString(4, lName);
			ps.setString(5, addr);
			ps.setString(6, phNo);
			ps.setString(7, mailId);
			ps.setInt(8, 2);
			int k = ps.executeUpdate();
			if (k == 1) {
				RequestDispatcher rd = req.getRequestDispatcher("Sample.html");
				rd.include(req, res);
				pw.println("<h3 class='tab'>User Registered Successfully</h3>");
			} else {
				RequestDispatcher rd = req.getRequestDispatcher("userreg");
				rd.include(req, res);
				pw.println("Sorry for interruption! Register again");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}