package servlets;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class BookDetailServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final String API_KEY = "AIzaSyCtQ66d531dJqKWgrLdfUclAuAEkgXz9JE"; // Remplacez par votre cl� API Google Books
    private static final String BASE_URL = "https://www.googleapis.com/books/v1/volumes?q=";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String bookTitle = request.getParameter("title");
        if (bookTitle == null || bookTitle.isEmpty()) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Book title is required");
            return;
        }

        OkHttpClient client = new OkHttpClient();
        String url = BASE_URL + bookTitle + "&key=" + API_KEY;
        Request apiRequest = new Request.Builder().url(url).build();

        try (Response apiResponse = client.newCall(apiRequest).execute()) {
            if (!apiResponse.isSuccessful()) throw new IOException("Unexpected code " + apiResponse);
            String jsonResponse = apiResponse.body().string();

            JsonObject jsonObject = JsonParser.parseString(jsonResponse).getAsJsonObject();
            JsonObject volumeInfo = jsonObject.getAsJsonArray("items").get(0).getAsJsonObject().getAsJsonObject("volumeInfo");

            String title = volumeInfo.get("title").getAsString();
            String authors = volumeInfo.get("authors").toString();
            String description = volumeInfo.get("description").getAsString();
            String thumbnail = volumeInfo.get("imageLinks").getAsJsonObject().get("thumbnail").getAsString();
            String isbn = volumeInfo.getAsJsonArray("industryIdentifiers").get(0).getAsJsonObject().get("identifier").getAsString();

            response.setContentType("text/html");
            PrintWriter pw = response.getWriter();
            pw.println("<!DOCTYPE html \"-//W3C//DTD XHTML 1.0 Strict//EN\"");
            pw.println("  \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
            pw.println("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
            pw.println("<head>");
            pw.println("    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"/>");
            pw.println("	<link rel=\"stylesheet\" href=\"styles.css\">");
            pw.println("    <title>Google Books Embedded Viewer API</title>");
            pw.println("    <script type=\"text/javascript\" src=\"https://www.google.com/books/jsapi.js\"></script>");
            pw.println("    <script type=\"text/javascript\">");
            pw.println("      google.books.load();");
            pw.println("");
            pw.println("      function initialize() {");
            pw.println("        var viewer = new google.books.DefaultViewer(document.getElementById('viewerCanvas'));");
            pw.println("        viewer.load('ISBN:" + isbn + "');");
            pw.println("      }");
            pw.println("");
            pw.println("      google.books.setOnLoadCallback(initialize);");
            pw.println("    </script>");
            pw.println("</head>");
            pw.println("<body>");
            pw.println("    <div id=\"viewerCanvas\"></div>");
            pw.println("</body>");
            pw.println("</html>");
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "An error occurred while fetching book details");
        }
    }
}
