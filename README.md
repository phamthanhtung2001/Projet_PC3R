# Projet PC3R

Assurez vous tout d'abord d'avoir MySQL Workbench et Apache Tomcat 10.1 ainsi que Maven installé sur votre machine

Assurez vous également d'avoir un serveur MySQL installé localement ainsi que d'avoir installé MySQL connector J afin que les servlets puissent communiquer avec la base de données.

Vérifiez dans le Build Path -> Library si dans Classpath vous avez bien le jar mysql-connector-j-8.0.33.jar. Sinon allez au répertoire d'installation de votre MySQL Connector J et allez dans lib. Vous trouverez le jar dans ce répertoire

Pour initialiser la base de données, faites les query suivant dans le MySQL Workbench
create database onlinebookstore;

use onlinebookstore;

create table books(barcode varchar(100) primary key, name varchar(100), author varchar(100), price int, quantity int);

create table users(username varchar(100) primary key,password varchar(100), firstname varchar(100),
    lastname varchar(100),address text, phone varchar(100),mailid varchar(100),usertype int);

insert into books values('10101','1984','George Orwell',500,5);

insert into books values('10102','Le temps des tempêtes','Nicolas Sarkozy ',150,13);

insert into books values('10103','La supplication','Alexandra Alexievitch',124,360);

insert into users values('User','Password','First','User','My Home','42502216225','User@gmail.com',2);

insert into users values('Admin','Admin','Mr.','Admin','Haldia WB','9584552224521','admin@gmail.com',1);

insert into users values('mgmg','mgmg','Mgmg','u','high','1236547089','mgmg@gmail.com',2);

commit;

Sur les fichiers context.xml dans WebContent\META_INF (username="root" password="Tung210701") et IDatabase dans src\constants (public static final String PASSWORD = "Tung210701";), changez le champ password au password de votre serveur MySQL

Pour lancer l'application, veuillez d'abord vérifier si vous avez ajouté un serveur Apache Tomcat à Eclipse

Si oui, sélectionner le projet puis faites Run -> Run on Server et choisissez le serveur Apache Tomcat

Le nom d'utilisateur par défaut d'un user est: mgmg et mgmg
Le nom d'utilisateur par défaut d'un admin est: Admin et Admin